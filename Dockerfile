# Nikola on Docker
# Credits:
# https://gitlab.com/ItalianLinuxSociety/nikola-docker

FROM python:3.9-slim

# This will be read once by "pip install" and then removed.
COPY ./requirements.txt /tmp/requirements.txt

ARG UID=1000
ARG GID=1000

# Add dedicated user.
RUN groupadd --gid $GID dockola \
 && useradd  --uid $UID --gid $GID dockola --create-home \
 && mkdir --parents /app \
 && chown dockola:dockola   /app \
 && chown dockola:   /tmp/requirements.txt

WORKDIR /app

# From this moment, run as that dedicated low-privileged user.
USER dockola:dockola

RUN pip install --upgrade pip            --no-warn-script-location \
 && pip install -r /tmp/requirements.txt --no-warn-script-location \
 && rm             /tmp/requirements.txt

RUN python -m nikola plugin -i sass &> /dev/null

# Developers may want to run "nikola serve" and not just "nikola build".
EXPOSE 8000

# When the container is running should just serve the website.
# Note that developers may want to run just a "nikola build".
COPY ./entrypoint.sh /tmp/entrypoint.sh
ENTRYPOINT [ "/tmp/entrypoint.sh" ]
