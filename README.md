# Nikola Docker

Simplest Docker image for building a static website with [Nikola](https://github.com/getnikola/nikola).

You only need a bash-compatible shell.

Image already avalaible: https://hub.docker.com/r/mte90/dockola

## Build locally

Clone this repository, and put it in a directory containing a Nikola site.

Then, run this command to build the Docker image:

```
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) .

```

Finally, run this command to launch a Nikola command (inside a Nikola site directory):

```
docker run dockola build # a nikola command

```

### You can find a guide about Nikola's available commands in the [NikolaHandbook](https://getnikola.com/handbook.html)

## DEPENDENCIES

- bash
- docker

## Build image for upload it

* Download the repository
* `docker build -t dockola .`
* `docker tag dockola mte90/dockola` # yes only Mte90 can upload it on the hub
* `docker push mte90/dockola` # yes only Mte90 can upload it on the hub

## Pipeline autobuilding

This `gitlab-ci.yml` will build and automatically push the container image in GitLab's own registry following this rules:

Will build if one of these files are modified:
- .gitlab-ci.yml
- Dockerfile
- Entrypoint

...using this naming convention:
- `name:latest` if committed on default branch
- `name:tag` if tagged
- `name:branch-$CI_COMMIT_REF_SLUG` otherwise

<details>
<summary>brief Mermaid graph</summary>

```mermaid
graph TD
%%subgraph Building triggers
    ispipeline(Pipeline Source is Schedule)
    ispipeline --> |no| a01
    %%ispipeline --> |no| a01 & a02 & a03
    a01(Dockerfile exists in main folder)
    a01 --> |no| a03
    a03($DOCKERFILE_PATH is not null)
    a03 --> |no| a04
    a04(Pipeline Source is Push)
    a04 --> |yes| a05
    a05(Pipeline is triggered manually?)
subgraph .
    sub00(Building Rules)
    sub00 --> sub02
    subgraph ..
        sub02(At least one of these files has changed:)
        ssub01(.gitlab-ci.yml)
        ssub02(Dockerfile)
        ssub03(entrypoint.sh)
    end
end
        

%%end
nobuild((Don't Build))

nobuild2((Don't Build))
%%ispipeline --> |no| a03
%%a05 --> |no| nobuild1
a01 --> |yes| sub00
a03 --> |yes| sub00
a05 --> |no| nobuild2
a05 --> |yes| tsub01
a04 --> |no| tsub01


%%ssub01 & ssub02 & ssub03 --> |yes| tsub01
sub02 --> |yes| tsub01
sub02 --> |no| nobuild
subgraph ...
    tsub01(Naming Convention)
    tsub01 --> tsub02
    tsub02(Commited on default branch) --> |yes| tag02[Tag as latest]
    tsub02 --> |no| tsub03
    tsub03(Commit is linked to a new tag) --> |yes| tag03[Tag as $CI_COMMIT_REF_SLUG]
    tsub03 --> |no| tsub04
    tsub04(Commit arrived on any other branch) --> tag04[Tag as branch-$CI_COMMIT_REF_SLUG]
end
ispipeline --> |yes| nobuild
tag04 & tag02 & tag03 --> Build((Build))
```

</details>


## CONTRIBUTORS

- Fatualux (the "Docker hammerer")
- Mte90 (the "ILS Refactor Master" - original proposer and image mantainer)
- Bozzy (the "Old School Sysadmin")
- tucs (the "I'll answer to your _what if...?_ with a _why not!_ no matter what.")

## ACKNOWLEDGMENTS

This is a WIP project... Stay tuned!


## LICENSE

This Docker image is based on a Python image. See their license:

https://hub.docker.com/_/python/

https://docs.python.org/3/license.html

[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

This project itself is licensed under the GPLv3 license.

See LICENSE file for more details.
